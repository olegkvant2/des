#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "des.h"

static FILE *key_file, *input_file, *output_file;

#define ACTION_GENERATE_KEY "-g"
#define ACTION_ENCRYPT "-e"
#define ACTION_DECRYPT "-d"

#define DES_KEY_SIZE 8

int hex_to_int(char c){
        int first = c / 16 - 3;
        int second = c % 16;
        int result = first*10 + second;
        if(result > 9) result--;
        return result;
}

int hex_to_ascii(char c, char d){
        int high = hex_to_int(c) * 16;
        int low = hex_to_int(d);
        return high+low;
}

#include <locale.h>

int main(int argc, char* argv[]) {
	clock_t start, finish;
	double time_taken;
	unsigned long file_size;
	unsigned short int padding;
	int i, j;
	unsigned int iseed = (unsigned int)time(NULL);

	srand (iseed);

	setlocale(LC_ALL,"Russian");
	if (argc < 2) {
		printf("�� ���� ������� ��� ���� ��������.");
		return 1;
	}
	else if ((strcmp(argv[1], ACTION_ENCRYPT) == 0) || (strcmp(argv[1], ACTION_DECRYPT) == 0)) { // Encrypt or decrypt
		short int bytes_read;
		unsigned char* des_key;
		short int bytes_written, process_mode;
		unsigned long block_count = 0, number_of_blocks;
		unsigned char* data_block;
		unsigned char* processed_block;
		key_set* key_sets;

		if (argc != 5) {
			printf("���������� ������� ��������� (%d). ������������: des [-e|-d] ���� �������.���� ��������.����", argc);
			return 1;
		}

		des_key = (unsigned char*) malloc(8*sizeof(char));
		for(i = 0, j = 0;i < 8;i++)
		{
			des_key[i] = hex_to_ascii(argv[2][j], argv[2][j + 1]);
			j += 2;
		}
		
		input_file = fopen(argv[3], "rb");
		if (!input_file) {
			printf("��������� ������� ������� ����.");
			return 1;
		}

		output_file = fopen(argv[4], "wb");
		if (!output_file) {
			printf("��������� ������� �������� ����.");
			return 1;
		}
		
		data_block = (unsigned char*) malloc(8*sizeof(char));
		processed_block = (unsigned char*) malloc(8*sizeof(char));
		key_sets = (key_set*)malloc(17*sizeof(key_set));

		start = clock();
		generate_sub_keys(des_key, key_sets);
		finish = clock();
		time_taken = (double)(finish - start)/(double)CLOCKS_PER_SEC;
		
		if (strcmp(argv[1], ACTION_ENCRYPT) == 0) {
			process_mode = ENCRYPTION_MODE;
			printf("�������..\n");
		} else {
			process_mode = DECRYPTION_MODE;
			printf("����������..\n");
		}
		
		fseek(input_file, 0L, SEEK_END);
		file_size = ftell(input_file);

		fseek(input_file, 0L, SEEK_SET);
		number_of_blocks = file_size/8 + ((file_size%8)?1:0);
		
		
		start = clock();

		while(fread(data_block, 1, 8, input_file)) {
			block_count++;
			if (block_count == number_of_blocks) {
				if (process_mode == ENCRYPTION_MODE) {
					padding = 8 - file_size%8;
					if (padding < 8) {
						memset((data_block + 8 - padding), (unsigned char)padding, padding);
					}

					process_message(data_block, processed_block, key_sets, process_mode);
					bytes_written = fwrite(processed_block, 1, 8, output_file);

					if (padding == 8) {
						memset(data_block, (unsigned char)padding, 8);
						process_message(data_block, processed_block, key_sets, process_mode);
						bytes_written = fwrite(processed_block, 1, 8, output_file);
					}
				} else {
					process_message(data_block, processed_block, key_sets, process_mode);
					padding = processed_block[7];

					if (padding < 8) {
						bytes_written = fwrite(processed_block, 1, 8 - padding, output_file);
					}
				}
			} else {
				process_message(data_block, processed_block, key_sets, process_mode);
				bytes_written = fwrite(processed_block, 1, 8, output_file);
			}
			memset(data_block, 0, 8);
		}

		finish = clock();

		free(des_key);
		free(data_block);
		free(processed_block);
		fclose(input_file);
		fclose(output_file);

		time_taken = (double)(finish - start)/(double)CLOCKS_PER_SEC;
		printf("�������� %s. ���� �������: %lf ������.\n", argv[3], time_taken);
		return 0;
	} else {
		printf("����������� ��: %s. ������ �������� �� ���� [-e ��� -d ].", argv[1]);
		return 1;
	}

	return 0;
}
